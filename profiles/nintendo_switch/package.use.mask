# Unmask joystick driver
x11-base/xorg-drivers -input_devices_joystick

media-video/ffmpeg david

# Requires >=dev-libs/ell-0.39
net-wireless/bluez btpclient mesh

# Not supported on switch
sys-kernel/installkernel efistub grub refind uki ukify

# 2024-11-24 - requires net-libs/libqrtr-glib that does not build
net-misc/networkmanager modemmanager
